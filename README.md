Stalfos
======================

A little project for building front-end templates with html, scss, js, svg and gulp. Evolved from [this](https://github.com/4ndeh/base-front-end-project/).

Please only use as a base. 

[hankchizljaw.io](https://hankchizljaw.io)
